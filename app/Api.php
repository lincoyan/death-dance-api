<?php
/**
 * @author Lincoyan Palma A.
 */

namespace App;


class Api
{

    protected $user;

    /**
     * Set current authenticated user
     *
     * @param mixed $user
     */
    public function setUser(\App\User $user)
    {
        $this->user = $user;
    }

    /**
     * Get current authenticated user
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

}