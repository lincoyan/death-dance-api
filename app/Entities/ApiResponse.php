<?php
/**
 * Created by PhpStorm.
 * User: lincoyan
 */

namespace App\Entities;

class ApiResponse
{

    //HTTP codes
    const SUCCESS = 200;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const NOT_FOUND = 404;
    const SERVICE_UNAVAILABLE = 503;

    protected $code;
    protected $message;
    protected $data;

    public function __construct($code = self::SERVICE_UNAVAILABLE, $message = NULL, $details = [])
    {

        if($code) $this->setCode($code);
        if($message) $this->setMessage($message);
        if($details) $this->setData($details);

    }

    /**
     * @return array
     */
    public function toArray(){

        return [
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'data' => $this->getData()
        ];

    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @param $message
     * @throws \Exception
     */
    public function setMessage($message)
    {
        if(!$message){
            throw new \Exception("Can't set empty message");
        }

        $this->message = $message;
    }

    /**
     * @param null $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

}