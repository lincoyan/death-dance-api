<?php

namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    public $timestamps  = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uid', 'time_block', 'user_id', 'dancer_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
        'dancer_id'
    ];

    /**
     * User who created the appointment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * Dancer who took the appointment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dancer(){
        return  $this->belongsTo('App\Entities\Dancer');
    }

}
