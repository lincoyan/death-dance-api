<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Api;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Api', function ($app) {
            return new Api();
        });
    }
}
