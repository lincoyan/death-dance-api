<?php

namespace App\Http\Middleware;

use Closure;
use App\Entities\ApiResponse;
use App\Traits\AuthHelpers;
use App\Facades\Api;

class JWTAuthenticate
{

    use AuthHelpers;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {

            $apiResponse = new ApiResponse();

            $jwt = \JWTAuth::parseToken();
            $payload = $jwt->getPayload();
            $userId = $payload->get('sub');
            $user = \App\User::find($userId);
            if(!$user){
                abort(ApiResponse::UNAUTHORIZED, "The token owner has no access to this resource anymore");
            }

            Api::setUser($user);

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            $apiResponse->setCode(ApiResponse::UNAUTHORIZED);
            $apiResponse->setMessage('Token expired');

            return response($apiResponse->toArray(), $apiResponse->getCode());

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            $apiResponse->setCode(ApiResponse::UNAUTHORIZED);
            $apiResponse->setMessage('Token not valid');

            return response($apiResponse->toArray(), $apiResponse->getCode());

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            $apiResponse->setCode(ApiResponse::UNAUTHORIZED);
            $apiResponse->setMessage('Token is missing');

            return response($apiResponse->toArray(), $apiResponse->getCode());

        }

        return $next($request);
    }
}
