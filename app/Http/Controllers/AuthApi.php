<?php

/**
 * Dance with Death API
 * Restful API for appointments scheduling
 *
 * OpenAPI spec version: 1.0.0
 */


namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Traits\AuthHelpers,
    App\Entities\ApiResponse,
    App\User,
    Illuminate\Support\Facades\Hash;


class AuthApi extends Controller
{

    use AuthHelpers;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Operation authTokenGet
     *
     * Get Access Token.
     *
     *
     * @return Http response
     */
    public function authTokenGet(Request $request)
    {

        $response = new ApiResponse();
        $authCredentials = $this->extractIncomingCredentials($request);

        $consumer = User::where('email', $authCredentials['email'])->first();

        if(!$consumer || !Hash::check($authCredentials['password'], $consumer->password)){
            abort(ApiResponse::UNAUTHORIZED, "This credentials doesn't match with our records");
        }

        $jwt = \JWTAuth::fromUser($consumer);

        $response->setCode(ApiResponse::SUCCESS);
        $response->setMessage('Token successfully generated');
        $response->setData(['token' => $jwt]);

        return response($response->toArray(), $response->getCode());

    }
}
