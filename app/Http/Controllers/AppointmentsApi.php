<?php

/**
 * Dance with Death API
 * Restful API for appointments scheduling
 *
 * OpenAPI spec version: 1.0.0
 *
 * @author Lincoyan Palma A.
 */


namespace App\Http\Controllers;

use App\Facades\Api;
use App\Entities\ApiResponse;
use App\Entities\Appointment;
use App\Entities\Dancer;
use App\Traits\AppointmentCommons;
use \Illuminate\Database\QueryException;

use Carbon\Carbon;
use Illuminate\Http\Request;

class AppointmentsApi extends Controller
{

    use AppointmentCommons;

    /**
     * Work day starting hour
     */
    const START_HOUR = 9;

    /**
     * Work day ending hour
     */
    const END_HOUR = 17;


    /**
     * Operation appointmentPost
     *
     * Appointment schedule.
     *
     *
     * @return Http response
     */
    public function appointmentPost(Request $request)
    {

        $response = new ApiResponse();

        try {

            $this->validate($request, [
                'name'  => 'required',
                'phone' => 'required|digits_between:10,12',
                'email' => 'required|email',
                'date'  => 'required|date_format:Y-m-d',
                'hour'  => 'required|date_format:H',
            ]);

            $date = Carbon::parse($request->input('date'));
            $hour = ltrim($request->input('hour'), '0');

            app('db')->beginTransaction();

            $this->checkAppointmentAvailability($date->toDateString(), $hour);
            $data = $this->scheduleAppointment($request);

            $response->setCode(ApiResponse::SUCCESS);
            $response->setMessage('Appointment successfully scheduled');
            $response->setData($data);

            app('db')->commit();

        } catch (QueryException $e) {

            app('db')->rollback();

            $response->setCode($e->getCode());
            $response->setMessage($e->getMessage());

        }

        return response($response->toArray(), $response->getCode());

    }

    /**
     * Operation appointmentUidDelete
     *
     * Appointment delete.
     *
     * @param string $uid The appointment unique identifier (required)
     *
     * @return Http response
     */
    public function appointmentUidDelete(Request $request, $uid)
    {

        //TODO Design a confirmation flow in order to mitigate the incompatibility of the DELETE method  with REST arquitecture

        $user = Api::getUser();
        $appointment = $user->appointments()->where('uid', $uid)->first();

        if(!$appointment){
            abort(ApiResponse::NOT_FOUND, "The appointment was not found");
        }

        $appointment->delete();

        $response = new ApiResponse(ApiResponse::SUCCESS, "Appointment $uid successfully deleted");

        return response($response->toArray(), $response->getCode());

    }

    /**
     * Operation appointmentUidPatch
     *
     * Appointment update.
     *
     * @param string $uid The appointment unique identifier (required)
     *
     * @return Http response
     */
    public function appointmentUidPatch(Request $request, $uid)
    {

        $response = new ApiResponse();

        try {

            $this->validate($request, [
                'date' => 'date_format:Y-m-d',
                'hour' => 'date_format:H',
            ]);

            $inputs = $request->all();
            unset($inputs['uid']);

            app('db')->beginTransaction();

            $user = Api::getUser();
            $appointment = $user->appointments()->where('uid', $uid)->first();

            if (!$appointment) {
                abort(ApiResponse::NOT_FOUND, 'Appointment not found');
            }

            $dancer = $this->updateDancer($appointment->dancer, $request);
            $updatedAppointment = $this->updateAppointment($appointment, $dancer, $request);
            $updatedAppointment = Appointment::find($updatedAppointment->getKey());

            $response->setCode(ApiResponse::SUCCESS);
            $response->setMessage('Appointment updated succesfully');
            $response->setData($this->formatAppointmentForOutput($updatedAppointment));

            app('db')->commit();

        } catch (QueryException $e) {

            app('db')->rollback();

            $response->setCode($e->getCode());
            $response->setMessage($e->getMessage());

        }

        return response($response->toArray(), $response->getCode());

    }

    /**
     * Operation appointmentsGet
     *
     * Scheduled Appointments.
     *
     *
     * @return Http response
     */
    public function appointmentsGet(Request $request)
    {

        $this->validate($request, [
            'year'  => 'date_format:Y|required',
            'month' => 'date_format:m|required',
        ]);

        $year = $request->input('year');
        $month = str_pad($request->input('month'), 2, '0', STR_PAD_LEFT);

        $user = Api::getUser();
        $appointments = $user->appointments()->with('dancer')->where(\DB::raw('DATE_FORMAT(time_block, "%Y-%m")'),
            "$year-$month")->get();

        $grouped = $this->groupAppointmentsByYearMonthAndDay($appointments);
        $this->formatAvailableTimeBlocksForOutput($grouped, $year, ltrim($month, '0'));

        $result = [
            'year'         => $year,
            'month'        => $month,
            'appointments' => $grouped
        ];

        return response($result);

    }

    /**
     * Operation appointmentsAvailableGet
     *
     * Available blocks.
     *
     *
     * @return Http response
     */
    public function appointmentsAvailableGet(Request $request)
    {

        $this->validate($request, [
            'year'  => 'date_format:Y|required',
            'month' => 'date_format:m|required',
        ]);

        $year = $request->input('year');
        $month = $request->input('month');

        $availableTimeBlocks = $this->getMonthlyAvailableTimeBlocks($year, $month);
        $this->formatAvailableTimeBlocksForOutput($availableTimeBlocks, $year, $month);

        $result = [
            'year'                  => $year,
            'month'                 => $month,
            'available_time_blocks' => $availableTimeBlocks
        ];

        $response = new ApiResponse(
            ApiResponse::SUCCESS,
            'Available appointments retrieved successfully',
            $result);

        return response($response->toArray(), $response->getCode());


    }
}
