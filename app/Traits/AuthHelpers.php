<?php
/**
 * JWT Auth helpers
 *
 * @author Lincoyan Palma
 */

namespace App\Traits;

use Illuminate\Http\Request;
use App\Entities\ApiResponse;


trait AuthHelpers
{

    /**
     * Validates and extracts the auth credentials from the incoming request
     *
     * @param Request $request
     * @return string $token
     */
    protected function extractIncomingCredentials(Request $request){

        $authorization = $request->header('Authorization');
        if(!$authorization){
            abort(ApiResponse::UNAUTHORIZED, 'Auth header is missing');
        }

        $authPair = explode(' ', $authorization);
        if(!isset($authPair[1])){
            abort(ApiResponse::UNAUTHORIZED, 'Auth header has an invalid format');
        }

        $credentialsPair = explode(':', base64_decode($authPair[1]));
        if(!isset($credentialsPair[0]) || !isset($credentialsPair[1])){
            abort(ApiResponse::UNAUTHORIZED, 'Auth header has an invalid format');
        }

        $credentials = [
            'email' => $credentialsPair[0],
            'password' => $credentialsPair[1]
        ];

        return $credentials;

    }


}