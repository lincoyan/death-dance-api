<?php
/**
 * Common Helpers for Appointments API
 *
 * @author Lincoyan Palma A.
 */

namespace App\Traits;
use App\Facades\Api;
use App\Entities\Appointment;
use App\Entities\ApiResponse;
use App\Entities\Dancer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;


trait AppointmentCommons
{


    /**
     * Returns all free time blocks between two given dates
     *
     * @param string $from
     * @return array $availableBlocks
     */
    protected function getMonthlyAvailableTimeBlocks($year, $month){

        $date = Carbon::create($year, $month);

        $from = $date->copy()->firstOfMonth();
        $to = $date->copy()->lastOfMonth();

        $from = Carbon::parse($from);
        $to = Carbon::parse($to);

        $scheduledAppointments = Appointment::with('dancer')
            ->whereBetween(\DB::raw('DATE_FORMAT(time_block, "%Y-%m-%d")'),
                [$from->toDateString(), $to->toDateString()])
            ->get();

        $grouped = $this->groupAppointmentsByYearMonthAndDay($scheduledAppointments);
        $availableBlocks = [];

        for($day = $from->day; $day <= $to->day; $day++){

            if(!$this->isWorkingDay("$year-$month-$day")){
                $availableBlocks[$year][$month][$day] = [];
                continue;
            }

            $appointments = isset($grouped[$year][$month][$day]) ? $grouped[$year][$month][$day] : [];
            $availableBlocks[$year][$month][$day] = $this->getDailyAvailableTimeBlocks($appointments);
        }

        return $availableBlocks;

    }

    /**
     * Formats daily available blocks for api response
     *
     * @param $availableTimeBlocks
     * @param $year
     * @param $month
     */
    protected function formatAvailableTimeBlocksForOutput(&$availableTimeBlocks, $year, $month){

        if(!isset($availableTimeBlocks[$year][$month])){
            return;
        }

        $availableTimeBlocksForOutput = [];

        foreach ($availableTimeBlocks[$year][$month] as $day => $blocks) {

            foreach ($blocks as $key => $block){

                if(isset($block->dancer)){
                    $block->name = $block->dancer->name;
                    $block->email = $block->dancer->email;
                    $block->phone = $block->dancer->phone;
                }

                unset($blocks[$key]->dancer);
            }

            $availableTimeBlocksForOutput[] = [
                'day'         => $day,
                'time_blocks' => $blocks
            ];
        }

        $availableTimeBlocks = $availableTimeBlocksForOutput;
    }

    /**
     * Returns available time blocks of a given daily appointments array
     *
     * @param array $appointments
     * @return array
     */
    protected function getDailyAvailableTimeBlocks($appointments = []){

        $availableBlocks = [];

        $startHour = defined('static::START_HOUR') ? self::START_HOUR : 9;
        $endHour = defined('static::END_HOUR') ? self::END_HOUR : 17;

        for($i = $startHour; $i <= $endHour; $i++){
            $availableBlocks[] = $i;
        }

        foreach ($appointments as $appointment){

            $timeBlock = Carbon::parse($appointment->time_block);
            $availableKey = array_search($timeBlock->hour, $availableBlocks);

            if($availableKey !== FALSE){
                array_pull($availableBlocks, $availableKey);
            }
        }

        return array_values($availableBlocks);
    }

    /**
     * Groups appointments by year, month and days
     *
     * @param Collection $appointments
     * @return array
     */
    protected function groupAppointmentsByYearMonthAndDay(Collection $appointments){

        $group = [];

        foreach ($appointments as $appointment){
            $block = Carbon::parse($appointment->time_block);
            $group[$block->year][$block->month][$block->day][] = $appointment;
        }

        return $group;
    }

    /**
     * Checks if a given date is a working day
     *
     * @param string $date
     * @return bool
     */
    protected function isWorkingDay($date){

        $date = Carbon::parse($date);
        if($date->dayOfWeek == Carbon::SATURDAY || $date->dayOfWeek == Carbon::SUNDAY){
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Checks if an appointment is available for a given date and hour
     *
     * @param $date
     * @param $hour
     */
    protected function checkAppointmentAvailability($date, $hour)
    {

        if(!$this->isWorkingDay($date)){
            abort(ApiResponse::BAD_REQUEST, 'Given date is not a working day');
        }

        if ($hour < self::START_HOUR || $hour > self::END_HOUR) {
            abort(ApiResponse::BAD_REQUEST, 'Given hour is out of office hours');
        }

        $appointments = Appointment::with('dancer')
            ->where(\DB::raw('DATE_FORMAT(time_block, "%Y-%m-%d")'), $date)
            ->get();

        $availableAppointments = $this->getDailyAvailableTimeBlocks($appointments);
        if (!in_array($hour, $availableAppointments)) {
            abort(ApiResponse::BAD_REQUEST, 'Appointment date and hour are already taken');
        }

    }

    /**
     * Creates a new appointment entry
     *
     * @param Request $request
     * @return array
     */
    protected function scheduleAppointment(Request $request){

        $date = Carbon::parse($request->input('date'));
        $hour = ltrim($request->input('hour'), '0');

        $dancer = Dancer::UpdateOrCreate(
            [
                'email' => $request->input('email')
            ],
            [
                'name'  => $request->input('name'),
                'phone' => $request->input('phone'),
            ]
        );

        $date->setTime($hour, 0, 0);

        $user = Api::getUser();
        $uid = md5(microtime());   //FIXME replace md5 hashing
        $user->appointments()->create([
            'uid'        => $uid,
            'time_block' => $date->toDateTimeString(),
            'dancer_id'  => $dancer->id
        ]);

        $data = [
            'appointment' => [
                'uid'        => $uid,
                'name'       => $dancer->name,
                'email'      => $dancer->email,
                'phone'      => $dancer->phone,
                'time_block' => $date->toDateTimeString()
            ]
        ];

        return $data;
    }

    /**
     * Updates a dancer or creates a new one if the email is different
     *
     * @param $dancer
     * @param Request $request
     * @return mixed
     */
    protected function updateDancer($dancer, Request $request){


        $inputs = $request->all();
        unset($inputs['email']);

        if($request->has('email') && ($dancer->email != $request->input('email'))){

            $email = $request->input('email');
            $dancerUpdateData = $inputs;
            $dancer = Dancer::UpdateOrCreate([
                'email' => $email
            ],$dancerUpdateData);

            return $dancer;
        }

        $dancer->update($inputs);

        return $dancer;
    }


    /**
     * Updates an existing appointment
     *
     * @param $appointment
     * @param null $dancer
     * @param Request $request
     * @return mixed
     */
    protected function updateAppointment($appointment, $dancer = NULL, Request $request){
        $appointmentUpdateData = [];
        $appointmentTimeBlock = Carbon::parse($appointment->time_block);

        $newDate = $request->has('date') ? $request->input('date') : $appointmentTimeBlock->toDateString();
        $newDate .= $request->has('hour') ?
            ' ' . str_pad($request->input('hour'), 2, '0' ,STR_PAD_LEFT) .':00:00' :
            ' ' . $appointmentTimeBlock->toTimeString();

        $newTimeBlock = Carbon::parse($newDate);

        if($appointmentTimeBlock->diffInHours($newTimeBlock)){
            $this->checkAppointmentAvailability($newTimeBlock->toDateString(), $newTimeBlock->hour);
            $appointmentUpdateData['time_block'] = $newTimeBlock->toDateTimeString();
        }

        if($dancer){
            $appointmentUpdateData['dancer_id'] = $dancer->id;
        }

        if(count($appointmentUpdateData)){
            $appointment->update($appointmentUpdateData);
        }

        return $appointment;

    }

    /**
     * Formats an existing appointment for displaying in an Api call response
     *
     * @param $appointment
     * @param $dancer
     * @return array
     */
    protected function formatAppointmentForOutput($appointment){
        return [
            'appointment' => [
                'uid'        => $appointment->uid,
                'name'       => $appointment->dancer->name,
                'email'      => $appointment->dancer->email,
                'phone'      => $appointment->dancer->phone,
                'time_block' => $appointment->time_block
            ]
        ];
    }

}