<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Entities\ApiResponse;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if($e instanceof NotFoundHttpException){

            $response = new ApiResponse(ApiResponse::NOT_FOUND, 'Resource  not found');
            return response($response->toArray(), $response->getCode());

        }

        if($e instanceof HttpException){

            $response = new ApiResponse($e->getStatusCode(), $e->getMessage());
            return response($response->toArray(), $response->getCode());

        }

        if($e instanceof \Illuminate\Validation\ValidationException){

            $response = new ApiResponse(ApiResponse::BAD_REQUEST, $e->getMessage(), json_decode($e->getResponse()->getContent()));
            return response($response->toArray(), $response->getCode());
        }

        return parent::render($request, $e);
    }
}
