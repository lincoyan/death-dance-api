<?php
/**
 * @author Lincoyan Palma A.
 */

namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class Api extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Api';
    }
}