/*jshint globalstrict: true*/
"use strict";

/**
 * Dance with dead API base URL
 *
 * @type {string}
 */
var apiBaseURL = 'http://45.55.62.86/v1/';

/**
 * Login modal element
 *
 * @type {JQuery}
 */
var loginModal = $('#loginModal');

/**
 * Add event modal element
 *
 * @type {JQuery}
 */
var eventModal = $('#eventModal');

/**
 * Loading container element
 *
 * @type {JQuery}
 */
var loadingSpinner = $('.loading');

/**
 *
 * @type {number}
 */
var loginAttempts = 0;

/**
 * Save token to session storage
 *
 * @param {string} token
 */
function setToken(token){
    sessionStorage.setItem('token', token);
}

/**
 * Retrieves token from session storage
 *
 * @returns {string} token
 */
function getToken(){
    return sessionStorage.getItem('token');
}

/**
 * Encodes a utf8 string to base64
 *
 * @param {string} str
 * @returns {string}
 */
function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}

/**
 * Creates a Basic Authorization header from user credentials
 *
 * @param email
 * @param password
 * @returns {{Authorization: string}}
 */
function makeAuthBasicHeaderFromCredentials(email, password){
    var credentials = email + ':' + password;
    var header = { 'Authorization': 'Basic ' + b64EncodeUnicode(credentials) };
    return header;
}

/**
 * Creates a Bearer Authorization header from token
 *
 * @param token
 * @returns {{Authorization: string}}
 */
function makeAuthBearerHeaderFromToken(token){
    var header = { 'Authorization': 'Bearer ' + token };
    return header;
}

/**
 * Makes a call to the auth API endpoint
 * to get an access JWT
 *
 * @param {string} email
 * @param {string} password
 * @param {function} successCallback
 * @param {function} errorCallback
 */
function login(email, password, successCallback, errorCallback){

    if(typeof successCallback === 'undefined'){
        successCallback = function(){};
    }

    if(typeof errorCallback === 'undefined'){
        errorCallback = function(){};
    }

    $.ajax({
        url: apiBaseURL + 'auth/token',
        type: 'get',
        headers: makeAuthBasicHeaderFromCredentials(email, password),
        success: successCallback,
        error: errorCallback
    }).always(function(){
        loadingSpinner.fadeOut('fast');
    });
}

/**
 * Makes a login attempt
 */
function attemptLogin(){

    var email = $('#login_email');
    var password = $('#login_password');
    var emailFormGroup = email.closest('.form-group');
    var passwordFormGroup = password.closest('.form-group');


    if(email.val() === ''){
        email.focus();
        emailFormGroup
            .addClass('has-error')
            .find('.help-block').text('Este campo es obligatorio');
        return;
    }

    if(password.val() === ''){
        password.focus();
        passwordFormGroup
            .addClass('has-error')
            .find('.help-block').text('Este campo es obligatorio');
        return;
    }

    emailFormGroup.removeClass('has-error').find('.help-block').text('');
    passwordFormGroup.removeClass('has-error').find('.help-block').text('');

    loadingSpinner.fadeIn();

    login(email.val(), password.val(), function(res){

        setToken(res.data.token);
        loginModal.modal('hide');
        loginAttempts = 0;
        loadEvents();

    }, function(res){

        var message = res.responseJSON.message;
        email.focus();
        emailFormGroup
            .addClass('has-error')
            .find('.help-block').text(message);

        loginAttempts++;

    });
}

/**
 * Initialize main events
 */
function eventsInit(){

    //Login form submit
    $('#loginBtn').on('click', function(){
        attemptLogin();
    });

    $('#login_email, #login_password').on('keyup', function(e){

        if(e.which === 13){
            attemptLogin();
        }
    });

    //Event add submit
    $('#eventBtn').on('click', function(){
        addEvent();
    })

    loginModal.on("hidden.bs.modal", function () {
        checkToken();
    });


}

/**
 * Checks if there is a token in session
 */
function checkToken(){

    var email = $('#login_email');
    var password = $('#login_password');
    var emailFormGroup = email.closest('.form-group');
    var passwordFormGroup = password.closest('.form-group');

    if(!getToken()){

        loadingSpinner.fadeOut();
        loginModal.modal('show');

        if(loginAttempts){
            email.focus();
            emailFormGroup
                .addClass('has-error')
                .find('.help-block')
                .text('You must login in order to continue');
        }

        lockCalendar();

        return;
    }

    loadingSpinner.fadeOut();
    unlockCalendar();

    emailFormGroup
        .removeClass('has-error')
        .find('.help-block')
        .text('');

    passwordFormGroup
        .removeClass('has-error')
        .find('.help-block')
        .text('');
}

/**
 * Initializes full calendar plugin
 */
function initCalendar(){
    $('#calendar').fullCalendar({
        header: { right: 'month,agendaDay, prev,next' },
        weekends: false,
        viewRender: function(){ loadEvents();},
        views: {
            day: { // name of view
                minTime: '09:00:00',
                maxTime: '18:00:00',
                // other view-specific options here
            }
        },
        dayClick: function(date, jsEvent, view) {


            $('#calendar').fullCalendar('changeView', 'agendaDay', date);

            if(date.hours() !== 0){
                var targetDate = date.format('YYYY-MM-DD HH:00:00');
                $('#time_block').val(targetDate);
                $('#eventModal').modal('show');
            }

        }
    });

}

/**
 * Append an overlay to calender in order to lock any interaction
 */
function lockCalendar(){
    unlockCalendar();
    $('#calendar').append('<div class="calendar-locked">You must login in order to continue.</div>');
}

/**
 * Removes locking overlay from calendar
 */
function unlockCalendar(){
    $('#calendar').find('.calendar-locked').remove();
}

/**
 * Loads events from the API depending on the current calendar's view
 */
function loadEvents(){

    checkToken();

    var moment = $('#calendar').fullCalendar('getDate');
    var year = moment.year();
    var month = moment.month() + 1;

    $.ajax({
        url: apiBaseURL + 'appointments',
        data: { year: year, month: month },
        type: 'get',
        headers: makeAuthBearerHeaderFromToken(getToken()),
        success: function(res){
            formatAllEvents(res.appointments);
        },
        error: function(res){
            if(res.responseJSON.code === 401){
                lockCalendar();
                loginModal.modal('show');
            }
        }
    }).always(function(){
        loadingSpinner.fadeOut('fast');
    });

}

/**
 * Get an array of events with format needed by the calendar plugin
 *
 * @param appointments
 */
function formatAllEvents(appointments){
    var events = [];
    for(var i in appointments) {
        if (typeof appointments[i] !== 'undefined') {
            formatDailyAppointments(appointments[i], events);
        }
    }
    $('#calendar').fullCalendar( 'removeEvents' );
    $('#calendar').fullCalendar( 'renderEvents', events );
}

/**
 * Iterates over every day of the month and sends
 * time blocks to formatting function
 *
 * @param appointments
 * @param events
 */
function formatDailyAppointments(appointments, events){
    for(var i in appointments.time_blocks){
        if (typeof appointments.time_blocks[i] !== 'undefined') {
            events.push(formatSingleEvent(appointments.time_blocks[i]));
        }
    }
}


/**
 * Converts a single time block to full calendar's start - end blocks format
 *
 * @param event
 * @returns {{title: string, start, end: string}}
 */
function formatSingleEvent(event){

    var title = event.name + ' - ' + event.email;
    var start = moment(event.time_block).format();
    var end = moment(event.time_block).add(1, 'h').format();

    return {
        title: title,
        start: start,
        end: end
    };


}


/**
 * Makes an API call to schedule an appointment
 */
function addEvent(){

    var time_block = $('#time_block');
    var name = $('#name');
    var email = $('#email');
    var phone = $('#phone');

    var date = moment(time_block.val());

    var data = {
        date: date.format('YYYY-MM-DD'),
        hour: date.format('H'),
        name: name.val(),
        email: email.val(),
        phone: phone.val()
    };

    loadingSpinner.fadeIn();

    $.ajax({
        url: apiBaseURL + 'appointment',
        data: data,
        type: 'post',
        headers: makeAuthBearerHeaderFromToken(getToken()),
        success: function(res){

            name.val('').closest('.form-group')
                .removeClass('has-error')
                .find('.help-block').text('');

            email.val('').closest('.form-group')
                .removeClass('has-error')
                .find('.help-block').text('');

            phone.val('').closest('.form-group')
                .removeClass('has-error')
                .find('.help-block').text('');

            loadEvents();
            eventModal.modal('hide');

        },
        error: function(res){
            var response = res.responseJSON;
            if(response.data !== null){
                var formGroup;
                for(var attr in response.data){
                    if(typeof response.data[attr] !== 'undefined'){
                        if(attr === 'date' || attr === 'hour'){
                            formGroup = time_block.closest('.form-group');
                        }else{
                            formGroup = $('#' + attr).closest('.form-group');
                        }
                        formGroup
                            .addClass('has-error')
                            .find('.help-block').text(response.data[attr][0]);
                    }

                }
            }
        }
    }).always(function(){
        loadingSpinner.fadeOut('fast');
    });

}

$(document).ready(function(){

    $.material.options.autofill = true;
    $.material.init();

    eventsInit();
    checkToken();
    initCalendar();

});