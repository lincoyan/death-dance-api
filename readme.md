# README #

Dance with death API v1.0

### What is this repository for? ###

* A Restful API for scheduling appointments to Dance with Death
* Version 1.0

### How do I get set up? ###

* You can find a Swagger config file in /docs folder
* You can load the config file by going to http://editor.swagger.io/ and copying/pasting the file contents
	or loading it from the file menu -> import file
* Once loaded, you can test every single API endpoint from othe swagger editor
* If you don't want to test the api from swagger's editor, you always can go direct to the client at http://45.55.62.86/client/

### Who do I talk to? ###

* Lincoyan Palma (lincoyan.palma@gmail.com)