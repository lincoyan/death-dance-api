<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     * @table appointments
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('uid', 32);
            $table->dateTime('time_block')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('dancer_id')->unsigned()->nullable();


            $table->foreign('user_id', 'fk_users_userid_appointments_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('dancer_id', 'fk_dancers_dancerid_appointments_idx')
                ->references('id')->on('dancers')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('appointments');
     }
}
