<?php

use Illuminate\Database\Seeder,
    Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'John Smith',
            'email' => 'john.smith@supermail.com',
            'password' => Hash::make('password'),
        ]);
    }
}
