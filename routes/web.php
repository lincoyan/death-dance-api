<?php

/**
 * Dance with Death API
 * Restful API for appointments scheduling
 *
 * OpenAPI spec version: 1.0.0
 */

/**
 * Dance with Death API
 * @version 1.0.0
 */

$app->get('/', function () use ($app) {
    return response([
        'name' => 'Dance with Death API',
        'version' => '1.0'
    ]);
});

/**
 * GET authTokenGet
 * Summary: Get Access Token
 * Notes: Returns a token to get access to the rest of the API's enpoints.
 * Output-Formats: [application/json]
 */
$app->GET('/v1/auth/token', 'AuthApi@authTokenGet');

$app->group(['middleware' => 'jwt'], function () use ($app) {

    /**
     * POST appointmentPost
     * Summary: Appointment schedule
     * Notes: Schedules an appointment to have a dance with Death.
     * Output-Formats: [application/json]
     */
    $app->POST('/v1/appointment', 'AppointmentsApi@appointmentPost');
    /**
     * DELETE appointmentUidDelete
     * Summary: Appointment delete
     * Notes: Deletes an existing appointment.
     * Output-Formats: [application/json]
     */
    $app->DELETE('/v1/appointment/{uid}', 'AppointmentsApi@appointmentUidDelete');
    /**
     * PATCH appointmentUidPatch
     * Summary: Appointment update
     * Notes: Updates partially or totally an existing appointment.
     * Output-Formats: [application/json]
     */
    $app->PATCH('/v1/appointment/{uid}', 'AppointmentsApi@appointmentUidPatch');
    /**
     * GET appointmentsGet
     * Summary: Scheduled Appointments
     * Notes: Get all the scheduled appointments for the current client.
     * Output-Formats: [application/json]
     */
    $app->GET('/v1/appointments', 'AppointmentsApi@appointmentsGet');
    /**
     * GET appointmentsAvailableGet
     * Summary: Available blocks
     * Notes: Returns all the available blocks of time for appointment scheduling.
     * Output-Formats: [application/json]
     */
    $app->GET('/v1/appointments/available', 'AppointmentsApi@appointmentsAvailableGet');
});



